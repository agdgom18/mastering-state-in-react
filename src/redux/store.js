import { configureStore } from '@reduxjs/toolkit';
import subscribe from './subsSlice/subscribeSlice';
import user from './fetchData/userSlice';

export const store = configureStore({
  reducer: {
    subscribe,
    user,
  },
});
