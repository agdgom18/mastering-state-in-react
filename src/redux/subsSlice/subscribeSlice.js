import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isSubscribed: false,
};

export const subscribeSlice = createSlice({
  name: 'subscribe',
  initialState,
  reducers: {
    toggleSubscribe(state, action) {
      state.isSubscribed = action.payload;
    },
  },
});

export const { toggleSubscribe } = subscribeSlice.actions;

export default subscribeSlice.reducer;
