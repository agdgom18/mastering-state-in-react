import React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import '../App.css';
const UserPage = () => {
  const params = useParams();
  const userInfo = useSelector((state) => state.user.users.find((el) => el.id === params.id));
  console.log(userInfo);

  return (
    <>
      <div style={{ margin: '0 auto' }} className="community__card">
        <img style={{ display: 'block', margin: '0 auto' }} src={userInfo.avatar} alt="logo" />
        <p className="card__desc">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
        <div style={{ textAlign: 'center' }} className="card__title ">
          <span>{userInfo.firstName} </span>
          <span>{userInfo.lastName}</span>
        </div>
        <h4 className="card__position">{userInfo.position}</h4>
      </div>
    </>
  );
};

export default UserPage;
