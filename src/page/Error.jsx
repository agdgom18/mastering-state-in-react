import React from 'react';
import { Link } from 'react-router-dom';

const Error = () => {
  return (
    <>
      <h1>Page Not Found</h1>
      <p>Looks like you've followed a broken link or entered a URL that does'not exist on this site.</p>

      <Link to="/">&larr; Back to our site</Link>
    </>
  );
};

export default Error;
