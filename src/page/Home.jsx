import React from 'react';
import Community from '../component/Community';
import Join from '../component/Join';

const Home = () => {
  return (
    <div>
      <Community />
      <Join />
    </div>
  );
};

export default Home;
