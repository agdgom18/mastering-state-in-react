import Community from '../component/Community';
import Error from '../page/Error';
import Home from '../page/Home';
import UserPage from '../page/UserPage';

const router = [
  {
    element: <Home />,
    path: '/',
  },
  {
    element: <Community />,
    path: '/community',
  },

  {
    element: <UserPage />,
    path: 'community/:id',
  },
  {
    element: <Error />,
    path: '*',
  },
];

export default router;
