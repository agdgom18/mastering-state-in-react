import React from 'react';

import { useDispatch } from 'react-redux';
import { toggleSubscribe } from '../redux/subsSlice/subscribeSlice';
const Join = () => {
  const [value, setValue] = React.useState('');
  const [btnValue, setBtnValue] = React.useState('SUBSCRIBE');
  const [isActive, setIsActive] = React.useState(true);

  const sendRequest = async (value, url, btn) => {
    try {
      const email = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: value }),
      });

      const response = await email.json();
      // case - email exist
      if (email.status === 422) {
        window.alert(response.error);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const dispatch = useDispatch();

  //  HAndler
  const handleChange = (e) => {
    e.preventDefault();
    const a = e.target.value;
    setValue(a);
    if (isActive) {
      sendRequest(value, 'http://localhost:3000/subscribe');
      setBtnValue('UNSUBSCRIBE');
      setIsActive(false);
      dispatch(toggleSubscribe(true));
    } else {
      setBtnValue('SUBSCRIBE');
      dispatch(toggleSubscribe(false));
      setIsActive(true);
      sendRequest(value, 'http://localhost:3000/unsubscribe');
    }
  };

  return (
    <div className="join">
      <h3 className="join-us--title">Join Our Program</h3>
      <p className="desc">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <form onSubmit={handleChange} className="form">
        {isActive && <input onChange={(e) => setValue(e.target.value)} defaultValue={value} className="input" placeholder="Email" />}
        <button data-testid="btn-test" className="btn" type="submit">
          {btnValue}
        </button>
      </form>
    </div>
  );
};

export default Join;
