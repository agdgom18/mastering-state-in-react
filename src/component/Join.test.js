import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import Join from './Join';
import configureStore from 'redux-mock-store';
import { toggleSubscribe } from '../redux/subsSlice/subscribeSlice';
const mockStore = configureStore([]);
jest.mock('../redux/subsSlice/subscribeSlice', () => ({
  toggleSubscribe: jest.fn(), // Мокаем экшен toggleSubscribe
}));
describe('Join component', () => {
  let store;

  beforeEach(() => {
    store = mockStore({ subscribe: { isSubscribed: false } });
  });

  it('should render correctly', () => {
    // Создаем начальное состояние для mock store
    const initialState = {
      subscribe: {
        isSubscribed: false, // Пример начального состояния
      },
    };

    // Создаем mock store с начальным состоянием
    const store = mockStore(initialState);

    const { getByText, getByTestId } = render(
      <Provider store={store}>
        <Join />
      </Provider>,
    );

    // Проводите проверки, связанные с рендерингом компонента
    expect(getByText('Join Our Program')).toBeInTheDocument();
    expect(getByText('SUBSCRIBE')).toBeInTheDocument();

    const subscribeButton = getByTestId('btn-test');
    expect(subscribeButton).toBeInTheDocument();
  });

  it('should handle change and dispatch toggleSubscribe accordingly', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <Join />
      </Provider>,
    );

    const subscribeButton = getByTestId('btn-test');
    console.log(subscribeButton.textContent);
    fireEvent.click(subscribeButton);
    console.log(subscribeButton.textContent);
    expect(subscribeButton.textContent).toBe('UNSUBSCRIBE');

    const actions = store.getActions();
    expect(actions).not.toEqual([{ type: toggleSubscribe.type }]);
  });
});
