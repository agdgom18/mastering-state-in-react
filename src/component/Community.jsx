import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers } from '../redux/fetchData/userSlice';
import { Link } from 'react-router-dom';

const Community = () => {
  const [active, setActive] = React.useState(true);
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  return (
    <>
      <>
        <h1 className="community__title">Big Community of People Like You </h1>
        <p className="community__desc">We’re proud of our products, and we’re really excited when we get feedback from our users.</p>

        <button
          style={{
            border: '2px solid red',
            padding: '20px 40px',
            backgroundColor: 'transparent',
            display: 'block',
            margin: ' 0 auto',
            marginBottom: '100px',
            cursor: 'pointer',
          }}
          onClick={() => setActive(!active)}>
          Hide Section
        </button>

        {
          <div data-testid="div-test" className="community__wrapper">
            {active &&
              user.users.map((el) => {
                return (
                  <div className="community__card" key={el.id}>
                    <img src={el.avatar} alt="logo" />
                    <p className="card__desc">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
                    <Link to={`community/${el.id}`}>
                      <div className="card__title ">
                        <span>{el.firstName} </span>
                        <span>{el.lastName}</span>
                      </div>
                    </Link>
                    <h4 className="card__position">{el.position}</h4>
                  </div>
                );
              })}
          </div>
        }
      </>
    </>
  );
};

export default Community;
