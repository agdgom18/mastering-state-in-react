import './App.css';
import React from 'react';

import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import router from './router/router';
import { store } from './redux/store';
import { Provider } from 'react-redux';
function App() {
  return (
    <div data-testid="test-id" className="app">
      <Provider store={store}>
        <RouterProvider router={createBrowserRouter(router)} />
      </Provider>
    </div>
  );
}

export default App;
