import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import ReactDOM from 'react-dom';
import App from './App';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import userReducer from './redux/fetchData/userSlice';
import { fetchUsers } from './redux/fetchData/userSlice';
import { act } from 'react-dom/test-utils';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import UserPage from './page/UserPage.jsx';

// 1
describe('userReducer', () => {
  it('should handle fetchUsers.pending', () => {
    const action = fetchUsers.pending();
    const state = { loading: false, users: [], error: '' };
    const newState = userReducer(state, action);
    expect(newState.loading).toBe(true);
  });

  it('should handle fetchUsers.fulfilled', () => {
    const action = fetchUsers.fulfilled([{ id: 1, name: 'John Doe' }]);
    const state = { loading: true, users: [], error: '' };
    const newState = userReducer(state, action);
    expect(newState.loading).toBe(false);
    expect(newState.users).toEqual([{ id: 1, name: 'John Doe' }]);
  });

  it('should handle fetchUsers.rejected', () => {
    const action = fetchUsers.rejected({ error: { message: 'Some error' } });
    const state = { loading: true, users: [], error: '' };
    const newState = userReducer(state, action);
    expect(newState.loading).toBe(false);
    expect(newState.users).toEqual([]);
  });
});
// 2
describe('should be render App', () => {
  it('renders Community component', () => {
    render(<App />);
    const element = screen.getByText(/People/i);
    expect(element).toBeInTheDocument();
  });
  it('renders join component', () => {
    render(<App />);
    expect(screen.getByText(/Join/i)).toBeInTheDocument();
  });
  it('button input should be rendered', () => {
    render(<App />);
    const buttonElement = screen.getByTestId('btn-test');
    expect(buttonElement).toBeInTheDocument();
  });
});
// 3
const middlewares = [thunk];
const mockStore = configureStore(middlewares);
describe('fetchUsers async action', () => {
  it('should dispatch fetchUsers.pending and fetchUsers.fulfilled actions', async () => {
    const store = mockStore({});

    // Mock the fetch function to return a response
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({}), // Mocking an empty response
      }),
    );

    // Dispatch the fetchUsers action
    await store.dispatch(fetchUsers());

    // Check if the actions were dispatched correctly
    const actions = store.getActions();
    expect(actions[0].type).toEqual(fetchUsers.pending().type);
    expect(actions[1].type).toEqual(fetchUsers.fulfilled().type);
  });
});
//4
const mockStore2 = configureStore([]);
describe('UserPage', () => {
  it('renders user info correctly', () => {
    const user = {
      id: '1',
      firstName: 'John',
      lastName: 'Doe',
      position: 'Developer',
      avatar: 'path/to/avatar.png',
    };

    const initialState = {
      user: {
        users: [user],
      },
    };

    const store = mockStore2(initialState);

    const { getByText, getByAltText } = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/users/1']}>
          <Routes>
            <Route path="/users/:id" element={<UserPage />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );

    // Check if user info is rendered correctly
    expect(getByText(user.firstName)).toBeInTheDocument();
    expect(getByText(user.lastName)).toBeInTheDocument();
    expect(getByText(user.position)).toBeInTheDocument();
    expect(getByAltText('logo')).toHaveAttribute('src', user.avatar);
  });
});
